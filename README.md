# jfxs / ci-toolkit

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Pipeline Status](https://gitlab.com/fxs/docker-ci-toolkit/badges/master/pipeline.svg)](https://gitlab.com/fxs/docker-ci-toolkit/pipelines)
[![Robot Framework Tests](https://fxs.gitlab.io/docker-ci-toolkit/all_tests.svg)](https://fxs.gitlab.io/docker-ci-toolkit/report.html)
[![Image size](https://fxs.gitlab.io/docker-ci-toolkit/docker.svg)](https://hub.docker.com/r/jfxs/ci-toolkit)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

A lightweight Docker image to easily build, automatically update Docker images with CI pipelines. A set of GitLab CI templates is also provided.

This image is automatically updated with the [Automated Continuous Delivery of Docker images](https://medium.com/@fx_s/automated-continuous-delivery-of-docker-images-b4bfa0d09f95) pattern.

## Getting Started

### Requirements

In order to run this container you'll need Docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

Experimental features are enabled.
An example of usage with multiarch and security scan is available [here](https://gitlab.com/fxs/docker-hello-world).

Steps:

* **Build image**:
  * [get-identical-tag.sh](#get-identical-tag.sh): get sha digest and all associated tags with the same sha for a tag.
  * [get-local-versions.sh](#get-local-versions.sh): create a file with the versions of the main components of the image.

* **Compare component version**:
  * [compare-versions.sh](#compare-versions.sh): Compare 2 version files.

* **Generate tag**:
  * [get-tag-index.sh](#get-tag-index.sh): get the next increment of a tag with the following format `<version>-<index>`.

* **Publish image**:
  * [set-dockerhub-tag.sh](#set-dockerhub-tag.sh): push an image to Dockerhub with multiple tags (version, major_version, latest).
  * [set-dockerhub-tag-multiarch.sh](#set-dockerhub-tag-multiarch.sh): push an image to Dockerhub with multiple tags (version, major_version, latest) in multiarch context.

* **Publish documentation**:
  * [versions-to-readme.sh](#versions-to-readme.sh): Insert a table in readme with the versions of the main components
  * [readme-to-dockerhub.sh](#readme-to-dockerhub.sh): update the web description of a Dockerhub repository.
  * [image-to-badge.sh](#image-to-badge.sh): generate a svg badge with size and layers details of the image.

### compare-versions.sh

compare-versions.sh is a shell program to compare two versions files and optionally detect if a file has changed on last commit.

```shell
Usage: compare-versions.sh [-d] -f <versions-file1>,<versions-file2 [-g file3]>
Options:
  -f 2 file paths separated by comma. Returns 1 if files are different, 0 otherwise
  -d display differences between files
  -g file name. Returns 1 if file has changed on last git commit, 0 otherwise
  -v | --version print version
  -h print usage

Example:
compare-versions.sh -f VERSIONS.latest,VERSIONS.new -g Dockerfile
```

#### get-identical-tag.sh

get-identical-tag.sh is a shell program to get a list of tag image with the same digest and optionaly extracts a substring of a matching tag with a regex pattern.

```shell
This script gets a list of tag image with the same digest and optionaly extracts a substring of a matching tag with a regex pattern.
The output has the following format:
sha256_digest tags_list_separated_by_comma [regex_output]

Usage: get-identical-tag.sh -r <repository> -u <username> -p <password> -t <tag-version> -a <architecture> [-i <variant>] [-x <regex>]
curl and jq are mandatory
Options:
  -r Repository. Official repository starts with library. Example: library/nginx
  -u Username of repository admin account
  -p Password of repository admin account
  -t Tag
  -a Architecture: amd64, arm, arm64, 386 ...
  -i Variant, usefull for arm architecture: v6, v7 ... (Not implemented)
  -x Regex to get a substring of a matching tag of the tags list
  -v | --version print version
  -h print usage

Example:
get-identical-tag.sh -r library/nginx -u dockerhubUser -p dockerhubPassword -t latest -a amd64 returns:
sha256:0efad4d09a419dc6d574c3c3baacb804a530acd61d5eba72cb1f14e1f5ac0c8f latest,mainline,1.19.0,1.19,1
with -x option:
get-identical-tag.sh -r library/nginx -u dockerhubUser -p dockerhubPassword -t stable-alpine -a arm64 -x \"\([0-9]*\.[0-9]*\.[0-9]*\)-alpine\" returns:
sha256:676b8117782d9e8c20af8e1b19356f64acc76c981f3a65c66e33a9874877892a stable-alpine,1.18.0-alpine,1.18-alpine 1.18.0
get-identical-tag.sh -r jfxs/ci-toolkit -u dockerhubUser -p dockerhubPassword -t latest -a amd64 -x \"\([0-9]*.[0-9]*.[0-9]\)-[0-9]*\" returns:
sha256:071939073647305a2b519cb5e83b6f90f3178d812f563f56eaed53b96147f726 latest,2.2,2,2.2.1-2 2.2.1
```

#### get-local-versions.sh

get-local-versions.sh is a shell program to generate a file with versions of installed packages.

```shell
Usage: get-local-versions.sh -f <from-image-digest> [-a <alpine-package1>,<alpine-package2>] [-p <python-package1>,<python-package2>] [-c <command1>,<command2>]  [-s <package1=version1>,<package2=version2>] [-d <versions-directory>]
Options:
  -f From image digest
  -a List of alpine packages separated by comma
  -p List of python packages separated by comma
  -c List of commands separated by comma. Version is set by the output of --version option of the cli
  -s List of specific packages with version separated by comma
  -d Directory in which the VERSIONS file is created. /etc by default
  -v | --version print version
  -h print usage

Example:
get-local-versions.sh -f ${IMAGE_FROM_SHA} -c docker -s ci-toolkit=${RELEASE_VERSION}
get-local-versions.sh -f ${IMAGE_FROM_SHA} -a python3 -p ansible-base,ansible-lint,jmespath,netaddr
```

#### get-tag-index.sh

get-tag-index.sh is a shell program to get the first available index for a Docker tag on Dockerhub.
Tag should have version-index format. Index starts at 1.

```shell
Usage: get-tag-index.sh -r <repository> -u <username> -p <password> -t <tag-version>
curl and jq are mandatory
Options:
  -r Repository
  -u Username of repository admin account
  -p Password of repository admin account
  -t Tag
  -v | --version print version
  -h print usage

Example:  2.8.0-1 and 2.8.0-2 are existing tags
get-tag-index.sh -r jfxs/ansible -u dockerhubUser -p dockerhubPassword -t 2.8.0 returns 3
```

#### image-to-badge.sh

image-to-badge.sh is a shell program to generate a badge for size and layers count of a Docker image like this:
[![Image size](https://fxs.gitlab.io/docker-ci-toolkit/docker.svg)](https://hub.docker.com/r/jfxs/ci-toolkit)

```shell
Usage: image-to-badge.sh -i <image> [-d <badges_directory>]
Options:
  -i Docker image
  -d badges directory, default current directory
  -v | --version print version
  -h print usage

Example:
image-to-badge.sh -i jfxs/ci-toolkit:latest -d public
```

#### readme-to-dockerhub.sh

readme-to-dockerhub.sh is a shell program to update full description of a Dockerhub repository.

```shell
Usage: readme-to-dockerhub.sh -r <repository> -u <username> -p <password> -f <filename>
curl and jq are mandatory
Options:
  -r Repository
  -u Username of repository admin account
  -p Password of repository admin account
  -f file path of the readme file
  -v | --version print version
  -h print usage

Example:
readme-to-dockerhub.sh -r jfxs/ci-toolkit -u dockerhubUser -p dockerhubPassword -f README.md
```

#### set-dockerhub-tag-multiarch.sh

set-dockerhub-tag-multiarch.sh is a shell program to publish images on Dockerhub with specific tags for architecture amd64 and arm64.

```shell
Usage: set-dockerhub-tag-multiarch.sh -u <username> -p <password> -i <source-image-amd64> -I <source-image-arm64> -r <image-repository> -t <tag> [-l] [-M] [-m]
curl and jq are mandatory

Options:
  -u username of repository admin account
  -p password of repository admin account
  -i source image amd64
  -I source image arm64
  -r image repository
  -t tag
  -l add tag latest
  -M add major version tag: 3.2.1-4 -> tag 3
  -m add minor version tag: 3.2.1-4 -> tag 3.2
  -v | --version print version
  -h print usage

Example with images build and pushed on gitlab.com registry:
set-dockerhub-tag-multiarch.sh -u dockerhubUser -p dockerhubPassword \
-i registry.gitlab.com/fxs/docker-hello-world/master-amd64:499694f5 -I registry.gitlab.com/fxs/docker-hello-world/master-arm64:499694f5 \
-r jfxs/hello-world -t 1.19.3-2 -l -M -m
```

#### set-dockerhub-tag.sh

set-dockerhub-tag.sh is a shell program to publish images on Dockerhub with specific tags.

```shell
Usage: set-dockerhub-tag.sh -u <username> -p <password> [-s <server>] -i <source-image> -r <image-repository> -t <tag> [-l] [-m]

Options:
  -u username of repository admin account
  -p password of repository admin account
  -s registry server. By default: docker.io (dockerhub)
  -i source image
  -r image repository
  -t tag
  -l add tag latest
  -M add major version tag: 3.2.1-4 -> tag 3
  -m add minor version tag: 3.2.1-4 -> tag 3.2
  -v | --version print version
  -h print usage

Example with images build and pushed on gitlab.com registry:
set-dockerhub-tag.sh -u dockerhubUser -p dockerhubPassword \
-i registry.gitlab.com/fxs/docker-hello-world/master-amd64:499694f5 -r jfxs/hello-world -t 1.19.3-2 -l -M -m
```

#### versions-to-readme.sh

versions-to-readme.sh is a shell program to set software versions table in a readme file.

```shell
Usage: versions-to-readme.sh -r <readme_file> -f <versions_file> -p <pattern>
Options:
  -r Path to readme file
  -f Path to version file
  -p Text pattern to replace by versions table in readme file
  -v | --version print version
  -h print usage

Example:
versions-to-readme.sh -r README.md -f ci_versions_file_new.txt -p "See versions on"
```

## Built with

Docker latest tag contains:

See versions on [Dockerhub](https://hub.docker.com/r/jfxs/ci-toolkit)

Versions of installed software are listed in /etc/VERSIONS. Example to see them for a specific tag:

```shell
docker run -t jfxs/ci-toolkit:1.2.1-1 cat /etc/VERSIONS
```

## Tests

Tests are runned with [Robot Framework](http://robotframework.org/).
Last test report is available [here](https://fxs.gitlab.io/docker-ci-toolkit/report.html).

## Vulnerability Scan

The Docker image is scanned every day with the open source vulnerability scanner [Trivy](https://github.com/aquasecurity/trivy).

The latest vulnerability scan report is available on [Gitlab Security Dashboard](https://gitlab.com/fxs/docker-ci-toolkit/-/security/dashboard/?state=DETECTED&state=CONFIRMED&reportType=CONTAINER_SCANNING).

## Find Us

* [Dockerhub](https://hub.docker.com/r/jfxs/ci-toolkit)
* [Gitlab](https://gitlab.com/fxs/docker-ci-toolkit)

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/docker-ci-toolkit/blob/master/LICENSE) file for details.

This program uses the Open Source/Free Software "curl" by Daniel Stenberg distributed under the MIT/X derivate license:

[https://curl.haxx.se/docs/copyright.html](https://curl.haxx.se/docs/copyright.html)
