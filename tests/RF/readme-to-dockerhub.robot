** Settings ***
Documentation     readme-to-dockerhub.sh tests
...
...               A test suite to validate readme-to-dockerhub shell script.

Library           OperatingSystem
Library           Process
Library           RequestsLibrary
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   readme-to-dockerhub.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -r
${REPOSITORY}   myRepository
${USERNAME}   Username
${PASSWORD}   password

*** Test Cases ***
with no option
    [Tags]    readme-to-dockerhub    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stderr}    -u argument is mandatory
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    readme-to-dockerhub    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option missing
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option with missing argument
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   -u   ${USERNAME}   -p   ${PASSWORD}   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-u] option missing
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -p   ${PASSWORD}   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -u argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-u] option with missing argument
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   -p   ${PASSWORD}   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option missing
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option with missing argument
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option missing
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option with missing argument
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -f
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option argument with wrong file path
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -f   ${TESTS_DIR}/files/Wrong_README.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: readme file
    And Should Contain    ${result.stderr}    Wrong_README.md does not exist!

[-v] option
    [Tags]    readme-to-dockerhub    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    readme-to-dockerhub    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    readme-to-dockerhub    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

Wrong option username
    [Tags]    readme-to-dockerhub    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   wrongUsername   -p   ${PASSWORD}   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to login to Dockerhub, response code: 401

Wrong option password
    [Tags]    readme-to-dockerhub    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   wrongPassword   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to login to Dockerhub, response code: 401

Wrong option repository
    [Tags]    readme-to-dockerhub    dockerhub
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   wrongRepository   -u   ${USERNAME}   -p   ${PASSWORD}   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Unable to set full description for wrongRepository on Dockerhub, response code: 404

Successfull update of readme on Dockerhub
    [Tags]    readme-to-dockerhub    dockerhub    sanity
    Given Create Session  dockerhub  https://hub.docker.com   disable_warnings=1
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -r   ${REPOSITORY}   -u   ${USERNAME}   -p   ${PASSWORD}   -f   ${TESTS_DIR}/files/README_f1_ref.md
    Then Should Be Equal As Integers    ${result.rc}    0
    ${resp}=  When GET On Session   dockerhub  /r/${REPOSITORY}
    Then Should Be Equal As Strings  ${resp.status_code}  200
