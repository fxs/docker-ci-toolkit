** Settings ***
Documentation     set-dockerhub-tag.sh tests
...
...               A test suite to validate set-dockerhub-tag shell script.

Library           Collections
Library           OperatingSystem
Library           Process
Library           RequestsLibrary

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}    set-dockerhub-tag.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -u
${USERNAME}   Username
${PASSWORD}   password
${SERVER}   docker.io
${IMAGE}     alpine
${TAG}        3.2.1-1
${TAG_MAJOR}        3
${TAG_MINOR}        3.2
${REPOSITORY}   myrepository
${SLEEP_TIME}   8s
${HUB_ENDPOINT}   https://hub.docker.com/v2

*** Test Cases ***
with no option
    [Tags]    set-dockerhub-tag    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -u argument is mandatory
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stderr}    -i argument is mandatory
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stderr}    -t argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    set-dockerhub-tag    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-u] option missing
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -u argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-u] option with missing argument
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option missing
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -p argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-p] option with missing argument
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-s] option with missing argument
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -s   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-i] option missing
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -i argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-i] option with missing argument
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option missing
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -r argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-r] option with missing argument
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-t] option missing
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -t argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-t] option with missing argument
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-v] option
    [Tags]    set-dockerhub-tag    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    set-dockerhub-tag    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

Docker command not found
    [Tags]    set-dockerhub-tag    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   wrongusername   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    docker command not found

Wrong username
    [Tags]    set-dockerhub-tag    dind-exp
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   wrongusername   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Impossible to login

Wrong password
    [Tags]    set-dockerhub-tag    dind-exp
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u  ${USERNAME}   -p   wrongpassword   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Impossible to login

Wrong server option
    [Tags]    set-dockerhub-tag    dind-exp
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u  user   -p   password   -s   example.com   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Impossible to login

Source image not found
    [Tags]    set-dockerhub-tag    dind-exp
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   wrongimage   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Impossible to pull source image

Wrong repository
    [Tags]    set-dockerhub-tag    dind-exp
    Given Remove existing tag   ${TAG}
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   xxx/wrong-repository   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Impossible to push image

Wrong tag
    [Tags]    set-dockerhub-tag    dind-exp
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   .wrongTag
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Impossible to tag image

Invalid tag for major version
    [Tags]    set-dockerhub-tag    dind-exp
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   latest   -M
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Invalid tag for major version

Successful publish a tag
    [Tags]    set-dockerhub-tag    dind-exp    sanity
    Given Remove existing tag   ${TAG}
    And Tag should not exist   ${TAG}
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    0
    And Sleep    ${SLEEP_TIME}
    And Tag should exist   ${TAG}

Successful publish latest tag
    [Tags]    set-dockerhub-tag    dind-exp
    Given Remove existing tag   latest
    And Tag should not exist   latest
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}   -l
    Then Should Be Equal As Integers    ${result.rc}    0
    And Sleep    ${SLEEP_TIME}
    And Tag should exist   latest

Successful publish major tag
    [Tags]    set-dockerhub-tag    dind-exp
    Given Remove existing tag   ${TAG_MAJOR}
    And Tag should not exist   ${TAG_MAJOR}
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}   -M
    Then Should Be Equal As Integers    ${result.rc}    0
    And Sleep    ${SLEEP_TIME}
    And Tag should exist   ${TAG_MAJOR}

Successful publish minor tag
    [Tags]    set-dockerhub-tag    dind-exp
    Given Remove existing tag   ${TAG_MINOR}
    And Tag should not exist   ${TAG_MINOR}
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}   -m
    Then Should Be Equal As Integers    ${result.rc}    0
    And Sleep    ${SLEEP_TIME}
    And Tag should exist   ${TAG_MINOR}

Successful publish a tag with server option
    [Tags]    set-dockerhub-tag    dind-exp
    Given Remove existing tag   ${TAG}
    And Tag should not exist   ${TAG}
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -u   ${USERNAME}   -p   ${PASSWORD}   -s   docker.io   -i   ${IMAGE}   -r   ${REPOSITORY}   -t   ${TAG}
    Then Should Be Equal As Integers    ${result.rc}    0
    And Sleep    ${SLEEP_TIME}
    And Tag should exist   ${TAG}

*** Keywords ***
Create Dockerhub session
    Create Session    dockerhub    ${HUB_ENDPOINT}    disable_warnings=1

Remove existing tag
    [Arguments]    ${tag}
    Create Dockerhub session
    &{headers} =    Create Dictionary    Content-Type=application/json
    &{data} =    Create Dictionary    username=${USERNAME}    password=${PASSWORD}
    ${resp} =    Post Request    dockerhub    /users/login/   data=${data}    headers=${headers}
    Should Be Equal As Integers    ${resp.status_code}    200
    ${token} =   Get Dictionary Values   ${resp.json()}   token
    Create Dockerhub session
    &{headers} =    Create Dictionary    Authorization=JWT ${token[0]}
    ${resp} =    Delete Request    dockerhub    /repositories/${REPOSITORY}/tags/${tag}/    headers=${headers}
    Should Be True    "${resp.status_code}" == "204" or "${resp.status_code}" == "404"

Tag should not exist
    [Arguments]    ${tag}
    Create Dockerhub session
    ${resp} =    Get Request    dockerhub    /repositories/${REPOSITORY}/tags/${tag}/
    Should Be Equal As Integers    ${resp.status_code}    404

Tag should exist
    [Arguments]    ${tag}
    Create Dockerhub session
    ${resp} =    Get Request    dockerhub    /repositories/${REPOSITORY}/tags/${tag}/
    Should Be Equal As Integers    ${resp.status_code}    200
