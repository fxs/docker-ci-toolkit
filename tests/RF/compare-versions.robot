** Settings ***
Documentation     compare-versions.sh tests
...
...               A test suite to validate compare-versions shell script.

Library           Process

*** Variables ***
${TESTS_DIR}   ${EXECDIR}/tests
${SHELL_DIR}   ${EXECDIR}/files
${SHELL.SH}   compare-versions.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} [-d]

*** Test Cases ***
with no option
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-v] option
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    compare-versions    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option with missing argument
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option: 2 different files with absolute path
    [Tags]    compare-versions    simple    sanity
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f2_ref.txt
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    1

[-f] option: 2 same content files
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f3_samef1.txt
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    0

[-f] option: 2 same content files but blank line
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f4_blankLinef1.txt
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    0

[-f] option: 2 same content files but different line order
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f5_orderf1.txt
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    0

[-d] option: 2 different content files
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   -f   ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f2_ref.txt
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    -alpine=3.10.1
    And Should Contain    ${result.stdout}    +alpine=3.10.2

[-d] option: 2 same content files
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   -f   ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f3_samef1.txt
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    No change

[-g] option with missing argument
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f2_ref.txt   -g
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-g] option: git not installed
    [Tags]    compare-versions    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH} -d -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f3_samef1.txt -g file2.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: git is not installed

[-g] option: Multiple file detected
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -d -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f3_samef1.txt -g file2.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: Multiple file2.txt detected

[-g] option: file has changed with same VERSIONS files
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f3_samef1.txt -g dir1/file2.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    1

[-g] option: file has not changed with same VERSIONS files
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f3_samef1.txt -g file1.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    0

[-g] option: file has changed with different VERSIONS files
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f2_ref.txt -g dir1/file2.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    1

[-g] option: file has not changed with different VERSIONS files
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f2_ref.txt -g file1.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    1

[-g] & [-d] option: file has changed with same VERSIONS files
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -d -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f3_samef1.txt -g dir1/file2.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    File dir1/file2.txt has changed!

[-g] & [-d] option: file has not changed with same VERSIONS files
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -d -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f3_samef1.txt -g file1.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Be Equal    ${result.stdout}    No change

[-g] & [-d] option: file has changed with different VERSIONS files
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -d -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f2_ref.txt -g dir1/file2.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    -alpine=3.10.1
    And Should Contain    ${result.stdout}    +alpine=3.10.2
    And Should Contain    ${result.stdout}    File dir1/file2.txt has changed!

[-g] & [-d] option: file has not changed with different VERSIONS files
    [Tags]    compare-versions    root
    [Setup]   Install git
    ${result} =    When Run Process    cd /tmp/test-git && ${SHELL_DIR}/${SHELL.SH} -d -f ${TESTS_DIR}/files/version_f1_ref.txt,${TESTS_DIR}/files/version_f2_ref.txt -g file1.txt   shell=yes
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    -alpine=3.10.1
    And Should Contain    ${result.stdout}    +alpine=3.10.2
    And Should Not Contain    ${result.stdout}    has changed!

*** Keywords ***
Install git
    ${result} =    Run Process    apk --no-cache add ca-certificates git   shell=yes
    Should Be Equal As Integers    ${result.rc}    0
    ${result} =    Run Process    if ! [ -e /tmp/test-git ]; then cp -rf ${TESTS_DIR}/files/test-git /tmp; fi   shell=yes
    Should Be Equal As Integers    ${result.rc}    0
    ${result} =    Run Process    if ! [ -e /tmp/test-git/.git ]; then mv /tmp/test-git/git /tmp/test-git/.git; fi   shell=yes
    Should Be Equal As Integers    ${result.rc}    0
    ${result} =    Run Process    cd /tmp/test-git && git status   shell=yes
    Should Be Equal As Integers    ${result.rc}    0
