#!/bin/sh
#
# compare-versions.sh is a shell program to compare two versions files.
#
# Copyright (c) 2019 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION=__VERSION__
DIFF=0

FILES_ARG=""
FILE1_PATH=""
FILE2_PATH=""
GIT_FILE=""
TMP_FILE1_PATH="/tmp/f1.bak"
TMP_FILE2_PATH="/tmp/f2.bak"

usage() {
    echo "Usage: compare-versions.sh [-d] -f <versions-file1>,<versions-file2> [-g file3]"
    echo ""
    echo "Options:"
    echo "  -f 2 file paths separated by comma. Returns 1 if files are different, 0 otherwise"
    echo "  -d display differences between files"
    echo "  -g file name. Returns 1 if file has changed on last git commit, 0 otherwise"
    echo "  -v | --version print version"
    echo "  -h print usage"
    echo ""
    echo "Example:"
    echo "compare-versions.sh -f VERSIONS.latest,VERSIONS.new -g Dockerfile"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
}

check_files() {
    if [ -z "$FILES_ARG" ]; then
        echo "Error: -f argument is mandatory !" >&2
        usage
        exit 1
    fi
    FILE1_PATH=$(echo "$FILES_ARG" | awk -F, '{print $1}')
    FILE2_PATH=$(echo "$FILES_ARG" | awk -F, '{print $2}')

    if ! [ -e "$FILE1_PATH" ]; then
        echo "Error: file1 $FILE1_PATH does not exist!" >&2
        exit 1
    fi
    if ! [ -e "$FILE2_PATH" ]; then
        echo "Error: file2 $FILE2_PATH does not exist!" >&2
        exit 1
    fi

    # Remove unprintable characters, blank lines, order lines.
    sed 's/[^[:print:]\t]//g' "$FILE1_PATH" | sed '/^[[:space:]]*$/d' | sort -u >"$TMP_FILE1_PATH"
    sed 's/[^[:print:]\t]//g' "$FILE2_PATH" | sed '/^[[:space:]]*$/d' | sort -u >"$TMP_FILE2_PATH"
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts df:g:hv option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    d)
        DIFF=1
        ;;
    f)
        FILES_ARG="${OPTARG}"
        ;;
    g)
        GIT_FILE="${OPTARG}"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

check_files

if [ "$DIFF" != 1 ]; then
    diff "$TMP_FILE1_PATH" "$TMP_FILE2_PATH" >/dev/null 2>&1 && RELEASE=0 || RELEASE=1
else
    diff "$TMP_FILE1_PATH" "$TMP_FILE2_PATH" && RELEASE=0 || RELEASE=1
fi

rm -f "$TMP_FILE1_PATH" "$TMP_FILE2_PATH"

if [ -n "$GIT_FILE" ]; then
    if ! [ -x "$(command -v git)" ]; then
        echo "Error: git is not installed." >&2
        exit 1
    fi

    GIT_DIFF_COUNT=$(git diff --name-only HEAD~1 HEAD | grep -c "$GIT_FILE")

    if [ "$GIT_DIFF_COUNT" -gt 1 ]; then
        echo "Error: Multiple $GIT_FILE detected! Use directory to differentiate" >&2
        exit 1
    fi

    if [ "$GIT_DIFF_COUNT" = 1 ]; then
        if [ "$DIFF" = 1 ]; then
            RELEASE="File $GIT_FILE has changed!"
        else
            RELEASE="1"
        fi
    fi
fi

if [ "$RELEASE" = 0 ] && [ "$DIFF" = 1 ]; then
    RELEASE="No change"
fi

echo "$RELEASE"
